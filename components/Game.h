//
// Created by havep on 9/6/17.
//

#ifndef CSAMPLE_GAME_H
#define CSAMPLE_GAME_H

#include <memory.h>
#include <stdlib.h>
#include <assert.h>
#include "Player.h"

typedef struct {
    Player * player_a;
    Player * player_b;
    int round_count;
} Game;

/*! create a new game;
 *
 * @param player_a
 * @param player_b
 * @return
 */
 Game * game_new(Player * player_a, Player * player_b);

/*!
 * "Same players play again"
 * @return != 0 if an error occurs
 */
int game_play(Game * game);

/*!
 * destroy a game
 * @param game to destroy (*game will be set to NULL if function succeeded)
 * @return != 0 if an error occurs
 */
int game_delete(Game ** game);

#endif //CSAMPLE_GAME_H
