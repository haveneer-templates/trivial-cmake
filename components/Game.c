//
// Created by havep on 9/6/17.
//

#include <time.h>
#include "Game.h"

/*-----------------------------*/

Game * game_new(Player * player_a, Player * player_b)
{
  Game * game = (Game*)malloc(sizeof(Game));
  game->player_a = player_a;
  game->player_b = player_b;
  game->round_count = 0;

  srand(time(NULL)); // init rand generator

  return game;
}

/*-----------------------------*/

int game_play(Game * game) {
  assert(game != NULL);

  int player_a_value, player_b_value;

  do {
    player_a_value = rand()%10000;
    player_b_value = rand()%10000;
  } while (player_a_value == player_b_value); // no draw game

  if (player_a_value > player_b_value) {
    // Player a wins
    player_registerRoundWin(game->player_a, player_a_value);
  } else {
    // Player b wins
    player_registerRoundWin(game->player_b, player_b_value);
  }

  ++game->round_count;

  return 0;
}

/*-----------------------------*/

int game_delete(Game ** game) {
  if (*game == 0)
    return 1;
  free(*game);
  *game = NULL;
  return 0;
}

/*-----------------------------*/
