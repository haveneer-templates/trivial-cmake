//
// Created by havep on 9/6/17.
//

#ifndef CSAMPLE_PLAYER_H
#define CSAMPLE_PLAYER_H

#include <memory.h>
#include <stdlib.h>
#include <assert.h>

typedef struct {
    char * nickname;
    int best_score;
    int win_count;
} Player;

/*! create a new player;
 *
 * @param name : player nickname
 * @return pointer new player or NULL if an error occurs
 */
Player * player_new(const char * name);

/*!
 * Register new score for the given player
 * @param player
 * @param score
 * @return != 0 if an error occurs
 */
int player_registerRoundWin(Player *player, int score);

/*!
 * destroy a player
 * @param player to destroy (*player will be set to NULL if function succeeded)
 * @return != 0 if an error occurs
 */
int player_delete(Player ** player);

#endif //CSAMPLE_PLAYER_H
