//
// Created by havep on 9/6/17.
//

#include "Player.h"

#include <memory.h>
#include <stdlib.h>
#include <assert.h>

/*-----------------------------*/

Player * player_new(const char * name)
{
  Player * p = (Player*)malloc(sizeof(Player));
  p->best_score = 0;
  p->win_count = 0;
  size_t len = strlen(name);
  p->nickname = (char*)malloc(sizeof(char)*(len+1));
  strcpy(p->nickname, name);
  return p;
}

/*-----------------------------*/

int player_registerRoundWin(Player *player, int score) {
  assert(player != NULL);
  if (player->best_score < score)
    player->best_score = score;
  ++player->win_count;
  return 0;
}

/*-----------------------------*/

int player_delete(Player ** player) {
  if (*player == 0)
    return 1;
  if ((*player)->nickname == 0)
    return 1;
  free((*player)->nickname);
  free(*player);
  *player = NULL;
  return 0;
}

/*-----------------------------*/
