#include <stdio.h>

#include "components/Player.h"
#include "components/Game.h"

// NB: j'utilise ouvertement le C99 avec sa possibilité de créer des variables pas uniquement en tete de bloc

int main() {
  printf("Welcome to this new 'Funny' Random Game!\n");

  // Init
  Player * me = player_new("Me");
  Player * you = player_new("You");
  Game * game = game_new(me,you);

  // Let's play
  for(int i=0;i<10;++i) {
    game_play(game);
  }

  // Results
  printf("Results after %d rounds\n", game->round_count);
  printf("%8s %10s %10s\n","Name", me->nickname,you->nickname);
  printf("%8s %10d %10d\n","Score", me->best_score,you->best_score);
  printf("%8s %10d %10d\n","#Win", me->win_count,you->win_count);

  // Cleanup
  if (game_delete(&game)) fprintf(stderr, "An error occurs while deleting game\n");
  assert(game == NULL);
  if (player_delete(&me)) fprintf(stderr, "An error occurs while player 'me'\n");
  assert(me == NULL);
  if (player_delete(&you)) fprintf(stderr, "An error occurs while player 'you'\n");
  assert(you == NULL);

  assert(player_delete(&you) != 0); // an player_delete occurs must occur here !

  return 0;
}